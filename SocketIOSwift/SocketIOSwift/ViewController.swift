//
//  ViewController.swift
//  SocketIOSwift
//
//  Created by Scott Leathers on 11/11/15.
//  Copyright © 2015 Scott Leathers. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let socket = SocketIOClient(socketURL: "50.97.183.16:15750")
        
        socket.on("connect") {data, ack in
            print("socket connected")
        }
        
        socket.on("message") {data, ack in
            print("socket message")
        }
        
        socket.onAny {
            print("Got event: \($0.event), with items: \($0.items)")
        }
        
        socket.on("error") {data, ack in
            print("socket error")
        }
            
        socket.connect()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


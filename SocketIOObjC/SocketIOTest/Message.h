//
//  Message.h
//  SocketIOTest
//
//  Created by Scott Leathers on 11/5/15.
//  Copyright © 2015 Scott Leathers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject
@property (nonatomic, copy) NSString *user;
@property (nonatomic, copy) NSString *message;
@end

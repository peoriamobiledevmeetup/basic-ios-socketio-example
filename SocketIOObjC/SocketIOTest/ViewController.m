//
//  ViewController.m
//  SocketIOTest
//
//  Created by Scott Leathers on 10/1/15.
//  Copyright © 2015 Scott Leathers. All rights reserved.
//

#import "ViewController.h"
#import "Message.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.messages = [[NSMutableArray alloc] init];
    
    socket = [[SocketIOClient alloc] initWithSocketURL:@"50.97.183.16:15750" opts:nil];
    
    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSLog(@"socket connected");
    }];
    
    [socket on:@"count" callback:^(NSArray* data, SocketAckEmitter* ack) {
        long totalUsers = [[data objectAtIndex:0] longValue];
        [self.totalCount setText:[NSString stringWithFormat:@"User Count: %ld", totalUsers]];
    }];
    
    [socket on:@"message" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //NSString *message = [data objectAtIndex:0];
        
        Message *msg = [[Message alloc] init];
        msg.message = [data objectAtIndex:0];
        
        [self.messages addObject:msg];
        [self.messageTable reloadData];
    }];
    
    [socket connect];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ([self.messages count] > 0 ? [self.messages count] : 0);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 22.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
    }
    
    Message *msg = [_messages objectAtIndex:indexPath.row];
    cell.textLabel.text = msg.message;

    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)touchSend:(id)sender {
    if ([[self.messageText text] length] > 0) {
        [socket emit:@"message" withItems:@[[_messageText text]]];
    }
    else {
        UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@""
                                                           message:@"You must enter a text message."
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [theAlert show];
    }
}
@end

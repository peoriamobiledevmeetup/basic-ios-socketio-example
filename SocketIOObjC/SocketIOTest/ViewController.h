//
//  ViewController.h
//  SocketIOTest
//
//  Created by Scott Leathers on 10/1/15.
//  Copyright © 2015 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Socket_IO_Client_Swift/Socket_IO_Client_Swift-Swift.h>

@interface ViewController : UIViewController {
    SocketIOClient* socket;
}

@property (strong, nonatomic) IBOutlet UITextField *messageText;
@property (strong, nonatomic) IBOutlet UILabel *totalCount;
@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) IBOutlet UITableView *messageTable;
- (IBAction)touchSend:(id)sender;

@end


